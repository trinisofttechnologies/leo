
Template.route.helpers({
	"route" : function(first){
		if(Session.get("route") == first){
			return true;
		}
	},
	"default" : function(){
		if(!app.router[Session.get("route")])
			return true;
	}
});
app.router = {
	"default" : "home",
	"#home" : "home",
	"#addhome" : "addhome",
	"#login" : "login",
	"#register" : "register",
	"#room" : "room",
	"#schedule" : "schedule",
	"#editroom" : "editroom",
}
app.routerFunction = {
	//"default" : "homepage",
	"#login" : function () {
    },
    "#schedule" : function () {
    },
	"#register" : function () {
	},
	"#home" : function (pause) {
	},
    "" : function (pause) {
	},
	"#addhome" : function(){

	},
	"#room" : function(arrgument){
		if(arrgument[1])
			Session.set("homeId",arrgument[1]);
	},
	"#editroom" : function(arrgument){
		if(arrgument[1])
			Session.set("editroomId",arrgument[1]);
	}
}
app.getRoute = function(){
	var hash = window.location.hash;
    if(app.router[hash])
    	return app.router[hash]
    else
    	return false;
}
app.onHashChange = function(){
	var hash = window.location.hash;
	hash = hash.split("/");
	Session.set("route",hash[0]);
	if(app.routerFunction[hash[0]])
		app.routerFunction[hash[0]](hash);
	else
		app.routerFunction["#home"]()
}
Router = {};
Router.go = function(hash){
	window.location.hash = hash;
}
$(window).on('hashchange',app.onHashChange);

// Router.go("home");

app.onHashChange();
