Template.roomSettings.roomSettings = function(){
	return Session.get("roomSettings");
}

Template.roomSettings.room = function(){
	
}

Template.roomSettings.events({
	"click .roomSettingsclose" : function(){
		Session.set("roomSettings",false);
		// GAnalytics.event("account","click #roomSettings");
	},
	"click .overlay" : function(){
		Session.set("roomSettings",false);
		// GAnalytics.event("account","click #roomSettings");
	},
    "click .dropdown" : function(e){
        if( $(e.currentTarget).find(".selectRoomType").hasClass("active")) {
            $(e.currentTarget).find(".selectRoomType").css("display","none");
            $(e.currentTarget).find(".selectRoomType").removeClass("active");
        }
        else {
            $(e.currentTarget).find(".selectRoomType").css("display","block");
            $(e.currentTarget).find(".selectRoomType").addClass("active");
        }
    },
	"click #roomSettings label i" : function(events){
		$(events.currentTarget).parent().siblings("input").addClass("active");
    	$(events.currentTarget).parent().siblings("input").removeAttr("readonly");
    	$(events.currentTarget).parent().siblings("input").focus();
	},
    "click .selectRoomType li" : function(events){
        $(".selectRoomType li").removeClass("selected");
        $(events.currentTarget).addClass("selected");
        $(events.currentTarget).parent().find(".selectRoomType").css("display","none");
        $(events.currentTarget).parent().find(".selectRoomType").removeClass("active");
        var typeId = $(events.currentTarget).attr("id");
        // console.log(typeId); 
        var cursorHome = Home.findOne();
        var homeId = Meteor.user().profile.homeId[0];
        var roomId = Session.get("roomSettingsId");
        if(cursorHome){
            var options = {"roomId":roomId,"homeId":homeId,"updateValue":typeId};
            Meteor.call("updateRoomType",options,function(){})        
            console.log(options)
        }
    },
	"blur input" :function(events){
		$(events.currentTarget).parent().siblings("input").removeClass("active");
    	$(events.currentTarget).parent().siblings("input").attr("readonly",true); 
    	var object = events.currentTarget;
    	if($(events.currentTarget).hasClass("roomName"))
        {
            var value = $(object).val();  
            // if(value.length>9)
            //     break;
            if(value){ 
            	console.log(value);
                var homeId = Meteor.user().profile.homeId[0];
            	var roomId = Session.get("roomSettingsId");
                var options = {"roomId":roomId,"homeId":homeId,"updateValue":value};
                Meteor.call("updateRoomName",options,function(){})                
            }
        }
        else if($(events.currentTarget).hasClass("floorNo"))
        {
            var value = $(object).val();  
            // console.log(value);
            if(value){        
                var homeId = Meteor.user().profile.homeId[0];
                var roomId = Session.get("roomSettingsId");
                var options = {"roomId":roomId,"homeId":homeId,"updateValue":value};
                Meteor.call("updateFloorNo",options,function(){})                
                // Meteor.users.update(this._id, {$set: {emails:value}});
            }
        }  	
	},
});