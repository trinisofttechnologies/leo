Template.buttonSettings.buttonSettings = function(){
	return Session.get("buttonSettings");
}
Template.buttonSettings.room = function(){
	if(Home.findOne()){
		return Home.findOne().room;	
	}
}

Template.buttonSettings.events({
	"click .buttonSettingsclose" : function(){
		Session.set("buttonSettings",false);
		// GAnalytics.event("account","click #roomSettings");
	},
	"click .overlay" : function(){
		Session.set("buttonSettings",false);
		// GAnalytics.event("account","click #roomSettings");
	},
	"click .dropdown" : function(e){
		if( $(e.currentTarget).find(".selectroom").hasClass("active")) {
            $(e.currentTarget).find(".selectroom").css("display","none");
            $(e.currentTarget).find(".selectroom").removeClass("active");
          }
          else {
            $(e.currentTarget).find(".selectroom").css("display","block");
            $(e.currentTarget).find(".selectroom").addClass("active");
          }
          if( $(e.currentTarget).find(".selectType").hasClass("active")) {
            $(e.currentTarget).find(".selectType").css("display","none");
            $(e.currentTarget).find(".selectType").removeClass("active");
          }
          else {
            $(e.currentTarget).find(".selectType").css("display","block");
            $(e.currentTarget).find(".selectType").addClass("active");
          }
	},
	"click #buttonSettings label i" : function(events){
		$(events.currentTarget).parent().siblings("input").addClass("active");
    	$(events.currentTarget).parent().siblings("input").removeAttr("readonly");
    	$(events.currentTarget).parent().siblings("input").focus();
	},
	"blur input" :function(events){
		$(events.currentTarget).parent().siblings("input").removeClass("active");
    	$(events.currentTarget).parent().siblings("input").attr("readonly",true); 
    	var object = events.currentTarget;
    	if($(events.currentTarget).hasClass("ButtonName"))
        {
            var value = $(object).val();
            if(value){ 
            	console.log(value.length);
                var homeId = Meteor.user().profile.homeId[0];
            	var roomId = Session.get("roomSettingsId");
            	console.log(app.editRelayName.Name)
                var options = {"Name":app.editRelayName,"homeId":homeId,"updateValue":value};
                Meteor.call("updateButtonName",options,function(){})                
            }
        }
        else if($(events.currentTarget).hasClass("floorNo"))
        {
            var value = $(object).val();  
            console.log(value);
            if(value){        
                var homeId = Meteor.user().profile.homeId[0];
                var roomId = Session.get("roomSettingsId");
                var options = {"roomId":roomId,"homeId":homeId,"updateValue":value};
                // Meteor.call("updateFloorNo",options,function(){})                
                // Meteor.users.update(this._id, {$set: {emails:value}});
            }
        }  	
	},
	"click .selectType li" : function(events){
		$(".selectType li").removeClass("selected");
		$(events.currentTarget).addClass("selected");
        $(events.currentTarget).parent().find(".selectType").css("display","none");
        $(events.currentTarget).parent().find(".selectType").removeClass("active");
        var typeId = $(events.currentTarget).attr("id");
        // console.log(typeId);	
		var cursorHome = Home.findOne();
        var homeId = Meteor.user().profile.homeId[0];
		if(cursorHome){
			var editRelayName = app.editRelayName.Name;
			var options = {"editRelayName": editRelayName};
			var options = {"Name":app.editRelayName,"homeId":homeId,"updateValue":typeId};
			Meteor.call("updateButtonType",options,function(){})		
			console.log(options)
		}
	},
	"click .selectroom li" : function(events){
        $(events.currentTarget).parent().find(".selectroom").css("display","none");
        $(events.currentTarget).parent().find(".selectroom").removeClass("active");

		var cursorHome = Home.findOne();
		if(cursorHome){
			var editRelayName = app.editRelayName.Name;
			var options = {"editRelayName": editRelayName};
			for(var i=0,il=cursorHome.room.length;i<il;i++){
				cursorHome.room[i].relay.push(app.editRelayName);
				// for(var j=0,jl=cursorHome.room[i].relay.length;j<jl;j++){
				// 	relay = cursorHome.room[i].relay[j];
				// 	if(relay){
				// 		if(relay.name == options.name)
				// 			return false;
				// 	}
				// }
			}		
			var homeId = Meteor.user().profile.homeId[0];
			delete cursorHome._id;
			Home.update({"_id":homeId},{$set : cursorHome})
		}
	},
});
app.confirmDuplicateButton = function(options){
	var cursorHome = Home.findOne();
	var relay = null;
	if(cursorHome){
		for(var i=0,il=cursorHome.room.length;i<il;i++){
			for(var j=0,jl=cursorHome.room[i].relay.length;j<jl;j++){
				relay = cursorHome.room[i].relay[j];
				if(relay){
					if(relay.name == options.editRelayName)
						return false;
				}
			}
		}		
	}		
	return true;
}