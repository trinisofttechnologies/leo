Template.room.relay = function(){
	var cursorHome = Home.findOne(); //{"_id" : Session.get("homeId")}
	var roomId = Session.get("homeId");
	if(cursorHome){
		for(var i=0,il=cursorHome.room.length;i<il;i++){
			if(cursorHome.room[i]._id == roomId){
				return cursorHome.room[i].relay;
			}
		}
		var array = [];
		for(var i=1,il=17;i<il;i++)
			array.push(cursorHome.relay["Relay"+i]);
		// console.log(array);
		return array;
	}
	else
		return [];
}

Template.room.events({
	"click .onOff" : function(){
		var options = {"homeId" : Session.get("homeId"),"relay" : this};
		Meteor.call("enableSwitch",options,function(){})
	},
	"click .buttonSettingsButton" : function(events){
		Session.set("buttonSettings",true);
		// Session.set("SettingsbuttonName",true);
		app.editRelayName = this;
		// GAnalytics.event("account","click #roomSettingsButton");
	},
});

app.dummyCallback = function(err,data){
	log(err);
	log(data);
}