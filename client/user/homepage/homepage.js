Template.home.home = function(){
	if(Meteor.user()){
		return Home.find({"_id" : {$in :Meteor.user().profile.homeId}});
	}
	else
		return []; 
}
Template.home.events({
	"click .roomSettingsButton" : function(events){
		Session.set("roomSettings",true);
		var roomId = this._id;
		console.log(roomId);
		Session.set("roomSettingsId",roomId);
		var homeId = Meteor.user().profile.homeId;
		console.log(Meteor.user().profile.homeId);
		homeId = Session.get("homeId");
		
		// GAnalytics.event("account","click #roomSettingsButton");
	},
})