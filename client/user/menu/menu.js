Template.menu.events({
    'click #menu a': function () {
     	app.closeMenu();
    },
    'click .menuOverlay': function () {
     	app.closeMenu();
    },
    'click #menuButton': function () {
    },
    "click #logoutButton" : function(){
    	app.logout();
    }
});
Template.menu.user = function(){
	return Meteor.user();
}
// var startPos = Matrix.translate(50,50,0);
// var endPos = Matrix.translate(200,300,0);
// var transform = new Modifier({transform: startPos});
// var easeTransition = { duration: 800, curve: EasingCurves.inOutBackNorm };
// transform.setTransform(endPos, easeTransition);
app.menu = function(){
    // /dfsdfsdf
	if($("#menuButton").hasClass("activeMenuButton")){
        app.famousMenuClose();
		// $("#menu").removeClass("myActive");
		$("#menuButton").removeClass("activeMenuButton");
		$("#header").css("z-index","2");
		// $(".menuOverlay").css("display","none");
	}
	else{
        app.famousMenuOpen();
		// $("#menu").addClass("myActive");
		// $("body").addClass("left pushed");
		$("#menuButton").addClass("activeMenuButton");
		// $(".menuOverlay").css("display","block");
		$("#header").css("z-index","0");
	}
}
app.closeMenu = function(){
	if($("#menuButton").hasClass("activeMenuButton")){
        app.famousMenuClose();
		// $("#menu").removeClass("myActive");
		$("#menuButton").removeClass("activeMenuButton");
		// $(".menuOverlay").css("display","none");
		$("#header").css("z-index","2");
	}
}
app.fbackblock = function(){
	$("#menuButton").css("display","none");
	$("#backButton").css("display","block");
}
app.backnone = function(){
	$("#menuButton").css("display","block");
	$("#backButton").css("display","none");
}
// $("#menu a").bind("click",app.closeMenu);
// $("#menuButton").bind("click",app.menu);
app.famousMenuOpenClose = function(){
    app.famousMenuOpen();
}
app.famousMenuClose = function(){
    var snapy = document.getElementById('menu');
    for(var i=100;i>0;i--){
        snapy.style["-webkit-transform"] = "translate3d(" +i +"%, 0px, 0px)";
        snapy.style["transform"] = "translate3d(" +i +"%, 0px, 0px)";
        snapy.style["-moz-transform"] = "translate3d(" +i +"%, 0px, 0px)";
    } 
}

app.famousMenuOpen = function(){
    var snapy = document.getElementById('menu');
    for(var i=0,il=100;i<il;i++){
        snapy.style["-webkit-transform"] = "translate3d(" +i +"%, 0px, 0px)";
        snapy.style["transform"] = "translate3d(" +i +"%, 0px, 0px)";
        snapy.style["-moz-transform"] = "translate3d(" +i +"%, 0px, 0px)";
    } 
}