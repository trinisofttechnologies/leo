Template.header.events({
    'click #menuButton': function () {
        app.menu();
    },
    'click #backButton': function () {
        window.history.back();
    }
});