Template.login.events({
    'click #registerform .submit': function () {
		app.register();
    },
    'click #goToRegister': function () {
		app.setRoute("#register");
    },
    'click #loginButton': function () {
		app.login();
    }
  });
app.user = null;
app.user = app.get("user");
app.beforeInfo = null;

app.registerCallback = function(err,success){
	if(err){
		app.visualEffect("registerButton",app.onError);
		$(".error").show();
		$(".error .header").html("Error");
		$(".error p").html(err.reason);
	}
	else{
	// if(success){
		app.visualEffect("registerButton",app.onSuccess);
		setTimeout(function(){Router.go("home")},2000);
	}
}


app.logout = function(){
	Meteor.logout();
}

app.login = function(event){
	if(event)
		event.preventDefault();
	app.visualEffect("loginButton",app.onLoad);
	var username = $("#loginUsername").val();
	var password = $("#loginPassword").val();
	Meteor.loginWithPassword(username,password, app.loginCallback);
	$(".error").hide();
	return false;
}

app.register = function(event){
	app.visualEffect("registerButton",app.onLoad);
	Accounts.createUser(app.setUserLogin(), app.registerCallback);
	$(".error").hide();
}

app.createUserCallback = function(err){
	app.showLoginError(err);
	if(!err)
		Router.go('homepage');
}

app.setUserLogin = function(){
	var username,email,password,homeId;

	username = $("#registerUsername").val();
	email = $("#registerEmail").val();
	password = $("#registerPassword").val();
	homeId = $("#selectHome").val();
	var user = {"username":username,"password":password,"email":email};
	user.profile = {};
	user.services = {};
	user.profile.homeId = [homeId];
	return user;
}

app.createUserCallback = function(err){
	app.showLoginError(err);
}

app.showLoginError = function(err){
	if(err){
		$("#loginMessage a").html(err.reason);
		$("#loginMessage").show();
	}
}

app.loginCallback = function(err){
	if(err){
		app.visualEffect("loginButton",app.onError);
		$(".error").show();
		$(".error .header").html("Error");
		$(".error p").html(err.reason);
	}
	else{
		app.visualEffect("loginButton",app.onSuccess);
		setTimeout(function(){Router.go('home');},2000);
	}
}

app.loginWithFacebook = function(){
	app.visualEffect("loginScreenFacebook",app.onLoad);
	if(app.phonegap){
		app.fbInit();
		app.fbNativeLogin();
	}
	else{
		app.visualEffect("loginScreenFacebook",app.onError);
	}
	
}
Meteor.loginAsFacebook = function(options, callback) {
	//create a login request with admin: true, so our loginHandler can handle this request
	options.myFacebook = true;
	//send the login request
	Accounts.callLoginMethod({
		methodArguments: [options],
		userCallback: callback
	});
};
app.facebookCallback = function(err){
	log(err);
	if(err){
		app.visualEffect("loginScreenFacebook",app.onError);
	}
	else{
		app.visualEffect("loginScreenFacebook",app.onSuccess);
		setTimeout(function(){Router.go("home")},2000);
	}
}

app.createFacebookUser = function(user,authResponse){
	var profilePictureUrl = null;
	if (user.picture.data) {
        profilePictureUrl = user.picture.data.url;
    } else {
        profilePictureUrl = user.picture;
    }

	var users = {"username":user.username,"email":user.email,"_id":user.id,"name":user.name};
	
	users.profile = {};
	users.services = {"facebook": {"token":authResponse.accessToken,"expire":authResponse.expirationTime,}};
	users.profile.words = app.userWords;
	users.profile.profile_picture  = profilePictureUrl;
	Meteor.loginAsFacebook(users,app.facebookCallback);
}
