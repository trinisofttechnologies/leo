Template.register.events({
    "click #registerButton" : function(event){
    	app.register(event);
    },
    "click .item.selectHome" : function(){
        $("#selectHome").val(this._id);
        $(".ui.fluid.selection.dropdown").removeClass("active visible");
        $(".ui.fluid.selection.dropdown .menu").removeClass("visible").addClass("hidden");
    },
    "click #dropdownRegister" : function(event){
    	if($("#dropdownRegister").hasClass("active"))
    	{
	    	$("#dropdownRegister").removeClass("active visible");
    		$("#dropdownRegister .transition").removeClass("active visible");
		}
		else{
	    	$("#dropdownRegister").addClass("active visible");
    		$("#dropdownRegister .transition").addClass("active visible");
		}
    	
    },
});

Template.register.home = function(){
    return Home.find({});
}