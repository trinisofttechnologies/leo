Template.addhome.events({
	"click #addHome" : function(){
		app.addHome();
	}
});

app.addHome = function(){
	app.visualEffect("addHome",app.onLoad);
	var homeName = $("#homeName").val();
	var roomNo = $("#roomNo").val();
	var homeIP = $("#homeIP").val();
	var homePort = $("#homePort").val();
	var homePass = $("#homePass").val();
	roomNo = Number(roomNo);
	var roomArray = [];
	for(var i=0;i<roomNo;i++){
		roomArray.push({"_id":Random.id(),"name":"Room"+i,"relay":[]});
	}
	var options = {"homeName" : homeName,
					"room" : roomArray,
					"homeIP" : homeIP,
					"homePort" : homePort,
					"homePass" : homePass
				};
	Meteor.call("addHome",options,app.addHomeCallback);
}

app.addHomeCallback = function(err,data){
	if(err)
		app.visualEffect("addHome",app.onError);
	else{
		app.visualEffect("addHome",app.onSuccess);
		setTimeout(function(){Router.go("home");},2000)
	}
		
}