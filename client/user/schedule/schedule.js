Template.schedule.relay = function(){
	var cursorHome = Home.findOne(); //{"_id" : Session.get("homeId")}
	var roomId = Session.get("homeId");
	
		var array = [];
		for(var i=1,il=17;i<il;i++)
			array.push(cursorHome.relay["Relay"+i]);
		return array;
}
Template.schedule.events({
	"click .dropdown" : function(e){
        if( $(e.currentTarget).find(".selectRelay").hasClass("active")) {
            $(e.currentTarget).find(".selectRelay").css("display","none");
            $(e.currentTarget).find(".selectRelay").removeClass("active");
        }
        else {
            $(e.currentTarget).find(".selectRelay").css("display","block");
            $(e.currentTarget).find(".selectRelay").addClass("active");
        }
    },
    "click #saveSchedule" : function(){
        app.saveSchedule();
    }
});

app.saveSchedule = function(){
    var options = {};
    options.date = {};
    var date = $("#dateSchedule").val();
    if(!date){
        log("no date found!");
        return;
    }
    date = date.split("-");
    options.date.year = date[0];
    options.date.month = date[1];
    options.date.day = date[2];
    
    var hour = $("#timeSchedule").val();
    if(!hour){
        log("no hours found!");
        return;
    }
    
    hour = hour.split(":");
    options.date.hour = hour[0];
    options.date.min = hour[1];
    options.dateSave = new Date(options.date.year, options.date.month, options.date.date, options.date.hour, options.date.min, 0);
    options.status = false;
    options.status = $("#scheduleOn").is(':checked')
    options.homeId = Session.get("homeId");
    Meteor.call("saveSchedule",options,app.dummyCallback);
}
