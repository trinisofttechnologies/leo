Package.describe({
  summary: "Special leo package"
});

Npm.depends({
	"xml2json" : "0.5.1",
	"node-schedule" : "0.1.13",
});

Package.on_use(function (api) {
	api.add_files('common.js', ['client',"server"]);
    api.add_files('client.js', 'client');
    api.add_files('server.js', 'server');
    api.add_files('lib/util.js', 'client');
	if(api.export){
		api.export(["app","collection","log"],['client',"server"]);
		api.export("schedule","server");
	}
});