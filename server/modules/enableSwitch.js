app.enableSwitch = function(options){
	var cursorHome = Home.findOne(options.homeId);
	var State = !options.relay.State;
	var relay = options.relay.Name;
	if(!relay)
		return;
	relay = relay.toLowerCase();
	relay = relay.charAt(0).toUpperCase() + relay.slice(1);
	if(State)
		State = 1;
	else
		State = 0;
	if(cursorHome){
		var url = "http://" +cursorHome.homeIP +":" +cursorHome.homePort +"/current_state.xml?pw=" +cursorHome.homePass +"&" +relay +"=" +State;
		var result = Meteor.http.get(url);			
		app.updateStatus(options.homeId,result);
	}
}

app.updateStatus = function(homeId,result){
	if(result){
		var content = result.content;
		content = app.parse.toJson(content);
		content = JSON.parse(content);
		var update = {};
		for(var i=1,il=17;i<il;i++){
			// log(content.CurrentState["Relay"+i].State);
			update["relay.Relay"+i +".State"] = content.CurrentState["Relay"+i].State;
		}
		Home.update(homeId,{$set : update});
		
	}
}