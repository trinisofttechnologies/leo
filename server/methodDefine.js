
app.updateRoomType = function(options){
	var cursorHome = Home.findOne(options.homeId);
	var updateFlag = false;
	if(cursorHome){
		for(i=0,il=cursorHome.room.length;i<il;i++){
			if(cursorHome.room[i]._id == options.roomId){
				cursorHome.room[i].type = options.updateValue;
				updateFlag = true;
			}
		}
		if(updateFlag){
			delete cursorHome._id;
			Home.update({"_id":options.homeId},{$set : cursorHome});
		}
	}
	else{
		log("room not found!");
	}
}
app.updateButtonType = function(options){
	var cursorHome = Home.findOne(options.homeId);// Name homeId new name
	var updateFlag = false;
	if(cursorHome){
		for(i=1,il=17;i<il;i++){
			if(cursorHome.relay["Relay"+i])
			{
				// console.log(cursorHome.relay["Relay"+i].Name+" "+options.updateValue)
				if(cursorHome.relay["Relay"+i].Name == options.Name.Name)
				{
					cursorHome.relay["Relay"+i].type = options.updateValue
					updateFlag = true;
				}
			}	
			
		}
		if(updateFlag){
			delete cursorHome._id;
			Home.update({"_id":options.homeId},{$set : cursorHome});
		}
	}
	else{
		log("room not found!");
	}
}
app.updateButtonName = function(options){
	// console.log(options.Name.Name);
	// options.Name.Name = options.Name.Name.toLowerCase();
	// options.Name.Name = options.Name.Name.toLowerCase();
	// options.Name.Name = options.Name.Name.charAt(0).toUpperCase() + options.Name.Name.slice(1);
	var cursorHome = Home.findOne(options.homeId);// Name homeId new name
	var updateFlag = false;
	if(cursorHome){
		for(i=1,il=17;i<il;i++){
			if(cursorHome.relay["Relay"+i])
			{
				// console.log(cursorHome.relay["Relay"+i].Name+ " "+ options.Name.Name +" "+options.updateValue)
				if(cursorHome.relay["Relay"+i].Name == options.Name.Name)
				{
					// console.log(cursorHome.relay["Relay"+i].NickName+ "***" + options.updateValue);
					cursorHome.relay["Relay"+i].NickName = options.updateValue;
					// console.log(cursorHome.relay["Relay"+i].NickName+ "***" + options.updateValue);
					updateFlag = true;
				}
			}				
		}
		if(updateFlag){
			delete cursorHome._id;
			Home.update({"_id":options.homeId},{$set : cursorHome});
			// console.log(cursorHome)	
		}
	}
	else{
		log("room not found!");
	}
}
app.updateRoomName = function(options){
	var cursorHome = Home.findOne(options.homeId);
	var updateFlag = false;
	if(cursorHome){
		for(i=0,il=cursorHome.room.length;i<il;i++){
			if(cursorHome.room[i]._id == options.roomId){
				cursorHome.room[i].name = options.updateValue;
				updateFlag = true;
			}
		}
		if(updateFlag){
			delete cursorHome._id;
			Home.update({"_id":options.homeId},{$set : cursorHome});
		}
	}
	else{
		log("room not found!");
	}
}
app.updateFloorNo = function(options){
	var cursorHome = Home.findOne(options.homeId);
	var updateFlag = false;
	if(cursorHome){
		for(i=0,il=cursorHome.room.length;i<il;i++){
			if(cursorHome.room[i]._id == options.roomId){
				cursorHome.room[i].floorNo = options.updateValue;
				updateFlag = true;
			}
		}
		if(updateFlag){
			delete cursorHome._id;
			Home.update({"_id":options.homeId},{$set : cursorHome});
		}
	}
	else{
		log("room not found!");
	}
}