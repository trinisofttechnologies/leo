Meteor.users.find({}).observe({
	"added" : function(insert){
		if(app.isAdmin(insert)){
			Roles.addUsersToRoles(insert._id, ['admin'])
		}
	}
});