Meteor.methods({
	"addHome" : function(options){
		return app.addHome(options)
	},
	"enableSwitch" : function(options){
		return app.enableSwitch(options);
	},
	"updateRoomName" :function(options){		
		return app.updateRoomName(options);
	},
	"updateFloorNo" :function(options){		
		return app.updateFloorNo(options);
	},
	"updateButtonName" :function(options){		
		return app.updateButtonName(options);
	},
	"updateButtonType" :function(options){		
		return app.updateButtonType(options);
	},
	"updateRoomType" :function(options){		
		return app.updateRoomType(options);
	},
	"saveSchedule" : function(options){
		return app.saveSchedule(options);
	}

});
